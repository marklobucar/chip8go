package mem

import (
	"fmt"
)

type ProgramStack struct {
	stack   []uint16
	pointer uint8
}

type Memory struct {
	layout []byte
	plen   int
	pc     *ProgramStack
}

func Make() *Memory {
	return &Memory{
		layout: make([]byte, 4096),
		pc: &ProgramStack{
			stack:   make([]uint16, 16),
			pointer: 0x0000,
		},
	}
}

func (mem *Memory) LoadProgram(cmds []byte, start uint16) {
	mem.plen = len(cmds)
	for i := 0; i < len(cmds); i++ {
		mem.Write(start+uint16(i), cmds[i])
	}
}

func (mem *Memory) Write(addr uint16, val byte) {
	if addr >= 0x000 && addr <= 0x1FF {
		panic(fmt.Sprintf("Attempted to access interpreter reserved address [%X]", addr))
	}
	mem.layout[addr] = val
}

func (mem *Memory) Read(addr uint16) byte {
	return mem.layout[addr]
}

func (mem *Memory) GetRange(start uint16, n byte) []byte {
	end := start + uint16(n)

	if end > 0xFFF {
		end = 0x1000
		start = 0xFFD
	}

	return mem.layout[start:end]
}

func (mem *Memory) ProgramLength() int {
	return mem.plen
}
