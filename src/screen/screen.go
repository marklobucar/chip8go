package screen

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"runtime"
)

// Pixel is an alias for boolean, with the added String() conversion method
// which returns █ if the pixel is true (on), or an empty space " " if it's off.
type Pixel bool

func (p Pixel) String() string {
	if p {
		return "█"
	}

	return " "
}

// Screen is a struct that represent a monochrome display used by the computers that ran Chip8.
// Usually it was 64 pixels wide and 32 pixels tall.
type Screen struct {
	Width  int
	Height int
	Pixels [][]Pixel
}

var clear map[string]func()

func init() {
	clear = make(map[string]func())
	clear["linux"] = func() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

// Make returns a new Screen struct of the given width and height with all the pixels initially off (false)
func Make(width, height int) *Screen {
	screen := make([][]Pixel, height)
	for i := range screen {
		screen[i] = make([]Pixel, width)
	}

	s := Screen{
		Pixels: screen,
		Width:  width,
		Height: height,
	}
	s.Clear()

	return &s
}

// Clear wipes the screen in the terminal
func (s *Screen) Clear() {
	value, ok := clear[runtime.GOOS]
	if ok {
		value()
	} else {
		panic("Cannot clear screen for unsupported platform")
	}
}

// Draw "draws" a sprite at the given location (x, y). Note that every sprite is automatically
// 8 pixels wide, and the height is equal to the length of the data array.
func (s *Screen) Draw(x, y byte, data []byte) bool {
	// fmt.Printf("Need to draw %v at %d, %d\n", data, x, y)

	var any_xord bool
	var row_xord bool
	var row []bool

	for i, b := range data {
		row_index := (y + byte(i)) % byte(s.Height)

		row = PixelsToBools(s.Pixels[row_index])
		row_xord = XorBoolRange(&row, ByteToBoolSlice(b), int(x))

		if !any_xord && row_xord {
			any_xord = row_xord
		}

		s.Pixels[row_index] = BoolsToPixels(row)
	}

	return any_xord
}

// DrawScreen prints out the current contents of the Pixels array in the screen
func (s *Screen) DrawScreen() {
	cmd := exec.Command("tput", "-S")
	cmd.Stdin = bytes.NewBufferString("cup 0 0")
	cmd.Stdout = os.Stdout
	cmd.Run()

	var sb bytes.Buffer

	for y := 0; y < s.Height; y++ {
		for x := 0; x < s.Width; x++ {
			sb.WriteString(s.Pixels[y][x].String())
		}
		sb.WriteString("\n")
	}

	fmt.Print(sb.String())
}

func PixelsToBools(pixels []Pixel) []bool {
	bools := make([]bool, len(pixels))

	for i, e := range pixels {
		bools[i] = bool(e)
	}

	return bools
}

func BoolsToPixels(bools []bool) []Pixel {
	pixels := make([]Pixel, len(bools))

	for i, e := range bools {
		pixels[i] = Pixel(e)
	}

	return pixels
}

func ByteToBoolSlice(n byte) []bool {
	ret := make([]bool, 8)
	sb := fmt.Sprintf("%08b", n)

	for i, e := range sb {
		if string(e) == "0" {
			ret[i] = false
		} else {
			ret[i] = true
		}
	}

	return ret
}

func BooleanXOR(a, b bool) (state, unset bool) {
	state = a != b
	unset = a && b

	return
}

func XorBoolRange(target *[]bool, source []bool, start int) (xord bool) {
	tlen := len(*target)

	for i, e := range source {
		idx := (start + i) % tlen // wraparound
		state, unset := BooleanXOR((*target)[idx], e)

		if !xord && unset {
			xord = true
		}

		(*target)[idx] = state
	}

	return
}
