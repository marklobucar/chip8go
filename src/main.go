package main

import (
	"bitbucket.org/marklobucar/chip8go/src/interpreter"
	"bitbucket.org/marklobucar/chip8go/src/parser"
)

func main() {
	path := "./maze.ch8"
	contents := parser.Chip8ToHex(path)

	itp := interpreter.New()
	itp.LoadProgram(contents)
	itp.Read()
}
