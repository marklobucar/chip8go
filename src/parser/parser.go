package parser

import (
	"os"
)

// Chip8ToHex takes a filepath to a valid Chip 8 program as input, parses the file, and returns the instructions it gets in sequence
func Chip8ToHex(filepath string) []byte {
	file, err := os.Open(filepath)
	if err != nil {
		panic(err.Error())
	}

	defer file.Close()

	filestat, err := file.Stat()
	if err != nil {
		panic(err.Error())
	}

	contents := make([]byte, filestat.Size())
	file.Read(contents)

	return contents
}
