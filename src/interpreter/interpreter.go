package interpreter

import (
	"encoding/binary"
	"math/rand"
	"time"

	"bitbucket.org/marklobucar/chip8go/src/mem"
	"bitbucket.org/marklobucar/chip8go/src/screen"
)

const (
	INITIAL_ADDRESS = 0x200
	MAX_ADDRESS     = 0xFFFF
	SLEEP_DURATION  = time.Millisecond * 1
)

func initRegisters() (m map[byte]byte) {
	m = make(map[byte]byte, 0xF)

	for i := byte(0x0); i <= 0xF; i++ {
		m[i] = 0x00
	}

	return
}

// Interpreter contains the basic memory and state of the program.
// This includes the program counter `pc`, the memory `mem`, the special register I `i` and the map of registers V0 through VF `registers`
type Interpreter struct {
	screen          *screen.Screen
	program_counter uint16
	memory          *mem.Memory
	register_I      uint16
	registers       map[byte]byte
}

// New returns a zeroed interpreter
func New() *Interpreter {
	return &Interpreter{
		screen:          screen.Make(64, 32),
		program_counter: INITIAL_ADDRESS,
		memory:          mem.Make(),
		register_I:      0x0000,
		registers:       initRegisters(),
	}
}

type Instruction struct {
	most_significant_byte byte
	x                     byte
	y                     byte
	n                     byte
	kk                    byte
	nnn                   uint16
}

// LoadProgram loads the bytes into the program memory, starting at mem address 0x200
func (i *Interpreter) LoadProgram(commands []byte) {
	i.memory.LoadProgram(commands, INITIAL_ADDRESS)
}

func (i *Interpreter) Read() {
	var even bool

	for ; ; i.program_counter += 0x1 {
		time.Sleep(SLEEP_DURATION)
		even = i.program_counter%2 == 0

		// fmt.Printf("Counter at %X, even %t\n", i.pc, even)

		if !even {
			// Proper instructions are located at even addresses
			continue
		}

		instruction := parse_instruction(i.memory.GetRange(uint16(i.program_counter), 2))

		switch instruction.most_significant_byte {
		case 0x1:
			i.program_counter = instruction.nnn - 0x1 // hack, needs fixing
		case 0x2:
		case 0x3:
			if i.Se(instruction.x, instruction.kk) {
				i.program_counter += 0x2
				// continue
			} else {
				// fmt.Println("Not skipping")
			}
		case 0x6:
			i.Ld(instruction.x, instruction.kk)
		case 0x7:
			i.Add(instruction.x, instruction.kk)
		case 0x8:
			switch instruction.n {
			case 0x0:
				i.LdXY(instruction.x, instruction.y)
			case 0x4:
				i.AddXY(instruction.x, instruction.y)
			case 0x5:
			case 0x7:
			}
		case 0xA:
			i.LdI(instruction.nnn)
		case 0xC:
			i.Rnd(instruction.x, instruction.kk)
		case 0xD:
			i.Drw(instruction.x, instruction.y, instruction.n)
		}
	}
}

func parse_instruction(command []byte) Instruction {
	most_significant_byte := command[0] >> 4

	x := command[0] & 0xF
	y := command[1] >> 4
	n := command[1] & 0xF
	kk := command[1]
	nnn := binary.BigEndian.Uint16(command) & 0x0FFF

	return Instruction{
		most_significant_byte,
		x,
		y,
		n,
		kk,
		nnn,
	}
}

// SetRegister sets the register Vx to the value of `kk`
func (i *Interpreter) SetRegister(vx, kk byte) {
	i.registers[vx] = kk
}

// GetRegister gets the value of the register Vx
func (i *Interpreter) GetRegister(vx byte) byte {
	return i.registers[vx]
}

// SetI sets the special register I to the value of `addr`
func (i *Interpreter) SetI(addr uint16) {
	i.register_I = addr
}

// GetI gets the current value of the special register I
func (i *Interpreter) GetI() uint16 {
	return i.register_I
}

func (i *Interpreter) Add(vx, kk byte) {
	sum := i.GetRegister(vx) + kk

	i.SetRegister(vx, sum%0xFF)
}

func (i *Interpreter) AddXY(vx, vy byte) {
	valx := i.GetRegister(vx)
	valy := i.GetRegister(vy)

	sum := valx + valy
	if sum%0xFF != 0 {
		i.Ld(0xF, 0x01)
	}

	i.SetRegister(vx, (sum % 0xFF))
}

// Ld sets the register Vx to the value kk
func (i *Interpreter) Ld(vx, kk byte) {
	i.SetRegister(vx, kk)
}

// LdXY sets the value of Vx to be equal to Vy
func (i *Interpreter) LdXY(vx, vy byte) {
	i.SetRegister(vx, i.GetRegister(vy))
}

// LdI sets the special register I to nnn
func (i *Interpreter) LdI(nnn uint16) {
	i.SetI(nnn)
}

// Rnd sets the value of Vx to a random byte ANDed with `kk`
func (i *Interpreter) Rnd(vx, kk byte) {
	rand.Seed(time.Now().UnixNano())
	rbyte := byte(rand.Uint32() & 0xFF)
	// fmt.Printf("Generated random byte %X\n", rbyte)
	rb := rbyte & kk
	i.Ld(vx, rb)
}

// Se checks to see if the register Vx has a value that is equal to kk.
// The next instruction should be skipped if this is true
func (i *Interpreter) Se(vx, kk byte) bool {
	return i.GetRegister(vx) == kk
}

// Drw The interpreter reads n bytes from memory, starting at the address stored in I. These bytes are then displayed as sprites on screen at coordinates (Vx, Vy). Sprites are XORed onto the existing screen. If this causes any pixels to be erased, VF is set to 1, otherwise it is set to 0. If the sprite is positioned so part of it is outside the coordinates of the display, it wraps around to the opposite side of the screen.
func (i *Interpreter) Drw(vx, vy, n byte) {
	startaddr := i.GetI()
	bdata := i.memory.GetRange(startaddr, n)

	x := i.GetRegister(vx)
	y := i.GetRegister(vy)

	if i.screen.Draw(x, y, bdata) {
		i.Ld(0xF, 0x1)
	} else {
		i.Ld(0xF, 0x0)
	}

	i.screen.DrawScreen()
}
