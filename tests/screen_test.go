package screen_test

import (
	"reflect"
	"testing"

	"bitbucket.org/marklobucar/chip8go/src/screen"
)

func Test_ScreenDraw(t *testing.T) {
	s := screen.Make(8, 2)

	xord := s.Draw(0, 0, []byte{128, 64})
	wantedXord := false

	if xord {
		t.Errorf("Test_ScreenDraw failed, xord = %v; wanted = %v", xord, wantedXord)
	}

	pixels := s.Pixels
	wantedPixels := [][]screen.Pixel{
		{true, false, false, false, false, false, false, false},
		{false, true, false, false, false, false, false, false},
	}

	if !reflect.DeepEqual(pixels, wantedPixels) {
		t.Errorf("Test_ScreenDraw failed, pixels = %v; wanted = %v", pixels, wantedPixels)
	}
}

func Test_XorBoolRange(t *testing.T) {
	type args struct {
		target *[]bool
		source []bool
		start  int
	}
	tests := []struct {
		name      string
		args      args
		wantXord  bool
		wantRange []bool
	}{
		{
			name: "64 pixel array xord with 128",
			args: args{
				target: &[]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				source: screen.ByteToBoolSlice(128),
				start:  32,
			},
			wantXord:  false,
			wantRange: []bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		},
		{
			name: "No wraparound, no collision, 128 to []bool",
			args: args{
				target: &[]bool{false, false, false, false, false, false, false, false},
				source: []bool{true, false, false, false, false, false, false, false},
				start:  0,
			},
			wantXord:  false,
			wantRange: []bool{true, false, false, false, false, false, false, false},
		},
		{
			name: "No wraparound, no collision, 64 to []bool",
			args: args{
				target: &[]bool{false, false, false, false, false, false, false, false},
				source: []bool{false, true, false, false, false, false, false, false},
				start:  0,
			},
			wantXord:  false,
			wantRange: []bool{false, true, false, false, false, false, false, false},
		},
		{
			name: "No wraparound, collision",
			args: args{
				target: &[]bool{true, false, false, false, false, false, false, false},
				source: []bool{true, false, false, false, false, false, false, false},
				start:  0,
			},
			wantXord:  true,
			wantRange: []bool{false, false, false, false, false, false, false, false},
		},
		{
			name: "Wraparound, no collision",
			args: args{
				target: &[]bool{false, false, false, false},
				source: []bool{true, true},
				start:  3,
			},
			wantXord:  false,
			wantRange: []bool{true, false, false, true},
		},
		{
			name: "Wraparound, collision",
			args: args{
				target: &[]bool{false, false, false, true},
				source: []bool{true, true},
				start:  3,
			},
			wantXord:  true,
			wantRange: []bool{true, false, false, false},
		},
		{
			name: "No wraparound, no collision, not the same length",
			args: args{
				target: &[]bool{false, false, false, false, false, false, false, false},
				source: []bool{true, false, true},
				start:  2,
			},
			wantXord:  false,
			wantRange: []bool{false, false, true, false, true, false, false, false},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotXord := screen.XorBoolRange(tt.args.target, tt.args.source, tt.args.start)

			if gotXord != tt.wantXord {
				t.Errorf("XorBoolRange() = %v, want %v", gotXord, tt.wantXord)
			}

			if !reflect.DeepEqual(tt.wantRange, (*tt.args.target)) {
				t.Errorf("Expected %v not equal to actual %v", tt.wantRange, tt.args.target)
			}
		})
	}
}

func Test_ByteToBoolSlice(t *testing.T) {
	type args struct {
		n byte
	}
	tests := []struct {
		name string
		args args
		want []bool
	}{
		{
			name: "128",
			args: args{
				n: 128,
			},
			want: []bool{true, false, false, false, false, false, false, false},
		},
		{
			name: "64",
			args: args{
				n: 64,
			},
			want: []bool{false, true, false, false, false, false, false, false},
		},
		{
			name: "32",
			args: args{
				n: 32,
			},
			want: []bool{false, false, true, false, false, false, false, false},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := screen.ByteToBoolSlice(tt.args.n); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ByteToBoolSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_bxor(t *testing.T) {
	type args struct {
		a bool
		b bool
	}
	tests := []struct {
		name      string
		args      args
		wantState bool
		wantUnset bool
	}{
		{
			name: "True ^ True",
			args: args{
				a: true,
				b: true,
			},
			wantState: false,
			wantUnset: true,
		},
		{
			name: "True ^ False",
			args: args{
				a: true,
				b: false,
			},
			wantState: true,
			wantUnset: false,
		},
		{
			name: "False ^ True",
			args: args{
				a: false,
				b: true,
			},
			wantState: true,
			wantUnset: false,
		},
		{
			name: "False ^ False",
			args: args{
				a: false,
				b: false,
			},
			wantState: false,
			wantUnset: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotState, gotUnset := screen.BooleanXOR(tt.args.a, tt.args.b)
			if gotState != tt.wantState {
				t.Errorf("BooleanXOR() gotState = %v, want %v", gotState, tt.wantState)
			}
			if gotUnset != tt.wantUnset {
				t.Errorf("BooleanXOR() gotUnset = %v, want %v", gotUnset, tt.wantUnset)
			}
		})
	}
}

func TestScreen_Draw(t *testing.T) {
	type args struct {
		x    byte
		y    byte
		data []byte
	}
	tests := []struct {
		name        string
		s           *screen.Screen
		args        args
		want_xord   bool
		want_pixels [][]screen.Pixel
	}{
		{
			name: "todo",
			s:    screen.Make(16, 16),
			args: args{
				x:    4,
				y:    0,
				data: []byte{128, 64, 32},
			},
			want_xord: false,
			want_pixels: [][]screen.Pixel{
				{false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
				{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.Draw(tt.args.x, tt.args.y, tt.args.data); got != tt.want_xord {
				t.Errorf("Screen.Draw() = %v, want %v", got, tt.want_xord)
			}

			if !reflect.DeepEqual(tt.s.Pixels, tt.want_pixels) {
				t.Errorf("Screen.Draw() wanted pixels = %v; got %v", tt.want_pixels, tt.s.Pixels)
			}
		})
	}
}
